<?php

Route::group(['middleware' => 'AuthMiddleware'], function() {
    Route::get('/user','UserController@index');
    Route::get('/logout','UserController@logout');
});
Route::get('/login', 'LoginController@index');
Route::post('/do-login','LoginController@doLogin');
Route::get('/register','RegisterController@index');
Route::post("/register/create",'RegisterController@create');

