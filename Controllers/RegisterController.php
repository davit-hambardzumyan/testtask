<?php

class RegisterController extends Controller {

    public function index() {
        setcookie('user_id', null, -1, '/');
        setcookie('user_name', null, -1, '/');
        setcookie('auth_user', null, -1, '/');
        $this->view('register');
    }

    public function create() {

        $name = $_POST['name'];
        $email = $_POST['email'];
        $password= $_POST['password'];
        $existingEmail = self::query("SELECT 'email' FROM users WHERE email='".$email."'");
        $errors = [];
        if(trim($name)==''){
            $errors[] = "Please enter Name";
        }
        if(trim($email)==''){
            $errors[] = "Please enter Email";
        }
        if(!empty($existingEmail)){
            $errors[] = "This email address has been registered, pelase enter valid email";
        }
        if(trim($password)==''){
            $errors[] = "Please enter Password";
        }
        if(empty($errors)){


        self::query("INSERT INTO users (name, email, password) VALUES ('".$name."','".$email."','".md5($password)."')");
        $user = self::query("SELECT * FROM users WHERE email='".$email."'");
        if(empty($user)){
            header("Location: /login");
            exit;
        }
            $userId = $user[0]['id'];
            $name = $user[0]['name'];
            setcookie("user_name", $name,3600,'/');
            setcookie("user_id", $userId,3600,'/');
            setcookie('toLogin',false);
            $_SESSION['user_id'] = $userId;
            return header("Location: /user");
    }
        return $this->view('register',['errors' => $errors]);
    }
}