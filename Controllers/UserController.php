<?php

class UserController extends Controller {

    public function index() {
        return $this->view('user-page');
    }

    public function logout(){

            setcookie('user_id', null, -1, '/register');
            setcookie('auth_user', null, -1, '/register');
            header("Location: login");
            exit;
    }

}