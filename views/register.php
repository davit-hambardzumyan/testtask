
<?php include 'header.php'?>
    <div class="container">
        <?php
        if(!empty($data['errors'])){
            foreach ($data['errors'] as $error){
                echo
                '<div class="alert alert-danger">
                      <strong>Danger!</strong> '.$error.'
                </div>';
            }
        }
        ?>
        <form method="post" action="/register/create">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" id="email" name="email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class="form-control" required>
            </div>
            <input type="submit" value="Register" class="btn btn-primary">
        </form>
    </div>

<?php include 'footer.php'?>