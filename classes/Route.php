<?php
class Route{

    public static $validRoutes = [];

    public static function get($route, $function) {
        self::$validRoutes[] = $route;

        if(($route==$_SERVER['REDIRECT_URL']) && $_SERVER['REQUEST_METHOD'] === 'GET'){
            $route = explode('@',$function);
            $class = new $route[0];
            $action = $route[1];

            call_user_func([$class,$action]);
        }
    }


    public static function post($route, $function){
        if($route==$_SERVER['REDIRECT_URL'] && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $route = explode('@', $function);
            $class = new $route[0];
            $action = $route[1];
            call_user_func([$class, $action]);

        }
    }

    public static function group($middleware,$callback){
        if(empty($_COOKIE['auth_user']) || $_COOKIE['auth_user']==false){
            $a = new $middleware['middleware'];
            setcookie('auth_user',$a->check());
        } else{
            if(is_callable($callback)){
                call_user_func($callback);
            }
        }



    }
}