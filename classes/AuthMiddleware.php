<?php

class AuthMiddleware {
    public function __construct() {
        if(empty($_COOKIE['user_id'])){
            setcookie('auth_user', null, -1);
        }
        session_start();
    }

    public function check(){

        if (!isset($_COOKIE['user_id']) && empty($_COOKIE['toLogin'])) {
            setcookie('toLogin',true);
            header("Location: login");
            return false;
        }else{
            return true;
        }
    }
}